package com.travel.bus.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Response model for the Bus Connectivity Service
 */
@XmlRootElement(name = "direct")
@XmlAccessorType(XmlAccessType.FIELD)
public class DirectBusResponse {

    public static final String departureStationIdQueryParamter = "dep_sid";
    public static final String arrivalStationIdQueryParamter = "arr_sid";
    public static final String directBusRouteQueryParamter = "direct_bus_route";

    @JsonProperty(departureStationIdQueryParamter)
    private Integer departureStationId;

    @JsonProperty(arrivalStationIdQueryParamter)
    private Integer arrivalStationId;

    @JsonProperty(directBusRouteQueryParamter)
    private Boolean isDirectBusRoute;

    public Integer getDepartureStationId() {
        return departureStationId;
    }

    public void setDepartureStationId(Integer departureStationId) {
        this.departureStationId = departureStationId;
    }

    public Integer getArrivalStationId() {
        return arrivalStationId;
    }

    public void setArrivalStationId(Integer arrivalStationId) {
        this.arrivalStationId = arrivalStationId;
    }

    public Boolean getDirectBusRoute() {
        return isDirectBusRoute;
    }

    public void setDirectBusRoute(Boolean directBusRoute) {
        isDirectBusRoute = directBusRoute;
    }

    public DirectBusResponse(Integer departureStationId, Integer arrivalStationId, Boolean isDirectBusRoute) {
        this.departureStationId = departureStationId;
        this.arrivalStationId = arrivalStationId;
        this.isDirectBusRoute = isDirectBusRoute;
    }

    public DirectBusResponse() {}
}
