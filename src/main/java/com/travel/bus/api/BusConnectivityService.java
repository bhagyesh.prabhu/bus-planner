package com.travel.bus.api;

import com.travel.bus.api.response.DirectBusResponse;
import com.travel.bus.api.response.ErrorResponse;
import com.travel.bus.io.SourceToDestinationsMapping;
import com.travel.bus.util.CommonUtils;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

/**
 * BusConnectivityService (exposed at "direct" path)
 */
@Path("direct")
public class BusConnectivityService {

    SourceToDestinationsMapping mapping = SourceToDestinationsMapping.getInstance();

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as json.
     *
     * @return json response.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public DirectBusResponse get(
            @NotNull @QueryParam(DirectBusResponse.departureStationIdQueryParamter) String depSidStr,
            @NotNull @QueryParam(DirectBusResponse.arrivalStationIdQueryParamter) String arrSidStr
    ) {
        Integer depSid = CommonUtils.validateQueryParam(DirectBusResponse.departureStationIdQueryParamter, depSidStr);
        Integer arrSid = CommonUtils.validateQueryParam(DirectBusResponse.arrivalStationIdQueryParamter, arrSidStr);

        if (depSid == arrSid) {
            throw new WebApplicationException(
                    Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
                            .entity(new ErrorResponse(HttpURLConnection.HTTP_BAD_REQUEST,  "Departure and arrival stations cannot be same"))
                            .build()
            );
        }

        return new DirectBusResponse(depSid, arrSid, mapping.isConnectivityDirect(depSid, arrSid));
    }
}
