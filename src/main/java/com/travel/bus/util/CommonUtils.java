package com.travel.bus.util;

import com.travel.bus.api.response.ErrorResponse;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.net.HttpURLConnection;

/**
 * Common utils for validation
 */
public class CommonUtils {

    public static Integer validateQueryParam(String queryParam, String value) {
        if (value == null) {
            throw new WebApplicationException(
                    Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
                            .entity(new ErrorResponse(HttpURLConnection.HTTP_BAD_REQUEST, queryParam + " query parameter is mandatory"))
                            .build()
            );
        }
        if (isNotInteger(value)) {
            throw new WebApplicationException(
                    Response.status(HttpURLConnection.HTTP_BAD_REQUEST)
                            .entity(new ErrorResponse(HttpURLConnection.HTTP_BAD_REQUEST, queryParam + " query parameter is not an integer"))
                            .build()
            );
        }
        return Integer.valueOf(value);
    }

    public static boolean isNotInteger(String str)
    {
        try
        {
            Integer.parseInt(str);
        }
        catch(NumberFormatException nfe)
        {
            return true;
        }

        return false;
    }
}
