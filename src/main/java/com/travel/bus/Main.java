package com.travel.bus;

import com.travel.bus.exception.LoadRoutesException;
import com.travel.bus.io.LoadRoutes;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8088/api/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), new ResourceConfig().packages("com.travel.bus"));
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.out.println("Usage: java -jar bus-planner-1.0.jar FILE (FILE - path of the routes file)");
            System.exit(1);
        }

        try {
            LoadRoutes.buildMappingFromRoutes(args[0]);
        } catch (LoadRoutesException e) {
            System.out.println("Error while loading the routes file: " + e);
            e.printStackTrace();
            System.exit(1);
        }

        final HttpServer server = startServer();
        System.out.println(String.format("Bus Connectivity Service started with WADL available at %sapplication.wadl", BASE_URI));
    }
}

