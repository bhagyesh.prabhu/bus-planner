package com.travel.bus.exception;

/**
 * Exception for the errors while loading the routes file.
 */
public class LoadRoutesException extends Exception {
    public LoadRoutesException(String message, Throwable cause) {
        super(message, cause);
    }
}