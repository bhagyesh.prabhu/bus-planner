package com.travel.bus.io;

import java.util.*;

/**
 * This singleton creates and maintain the source to destination mapping.
 */
public class SourceToDestinationsMapping {
    private static SourceToDestinationsMapping instance = null;

    private SourceToDestinationsMapping() {}

    public static synchronized SourceToDestinationsMapping getInstance() {
        if (instance == null) {
            instance = new SourceToDestinationsMapping();
        }
        return instance;
    }

    private Map<Integer, HashSet<Integer>> sourceToDestinations = new HashMap<Integer, HashSet<Integer>>();

    public void addDestinationsToStation(Integer stationId, List<Integer> destinationStationIds) {
        HashSet<Integer> destinations = sourceToDestinations.getOrDefault(stationId, new HashSet<Integer>());
        destinations.addAll(destinationStationIds);

        sourceToDestinations.put(stationId, destinations);
    }

    public boolean isConnectivityDirect(Integer departureStation, Integer arrivalStation) {
        return sourceToDestinations.getOrDefault(departureStation, new HashSet<Integer>()).contains(arrivalStation);
    }
}
