package com.travel.bus.io;

import com.travel.bus.exception.LoadRoutesException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * File loader for reading the file line by line and updating the Source to Destinations mapping
 */
public class LoadRoutes {

    private static SourceToDestinationsMapping mapping = SourceToDestinationsMapping.getInstance();

    public static void buildMappingFromRoutes(String filePath) throws LoadRoutesException {

        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            int routesCount = Integer.parseInt(br.readLine());

            for (int i = 0; i < routesCount; i++) {
                String route = br.readLine();
                List<Integer> stations = Arrays.stream(route.split("\\s+")).skip(1).map(Integer::valueOf).collect(Collectors.toList());
                stations.forEach(sId -> mapping.addDestinationsToStation(sId, stations));
            }
        } catch (Exception e) {
            throw new LoadRoutesException("Exception while loading routes to generate mapping. File: " + filePath, e);
        }
    }
}
