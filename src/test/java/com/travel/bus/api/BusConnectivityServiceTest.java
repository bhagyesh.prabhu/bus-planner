package com.travel.bus.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import com.travel.bus.Main;
import com.travel.bus.io.LoadRoutes;
import org.glassfish.grizzly.http.server.HttpServer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BusConnectivityServiceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        //load the routes
        LoadRoutes.buildMappingFromRoutes("src/test/resources/routes");
        // start the server
        server = Main.startServer();
        // create the client
        Client c = ClientBuilder.newClient();

        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testValidRoute() {
        Response response = target.path("direct").queryParam("dep_sid", 12).queryParam("arr_sid", 179).request().get();
        assertEquals(200, response.getStatus());
        assertEquals("{\"dep_sid\":12,\"arr_sid\":179,\"direct_bus_route\":true}", response.readEntity(String.class));
    }

    @Test
    public void testInvalidRoute() {
        Response response = target.path("direct").queryParam("dep_sid", 745).queryParam("arr_sid", 228).request().get();
        assertEquals(200, response.getStatus());
        assertEquals("{\"dep_sid\":745,\"arr_sid\":228,\"direct_bus_route\":false}", response.readEntity(String.class));
    }

    @Test
    public void testMissingDepSidParam() {
        Response response = target.path("direct").queryParam("arr_sid", 179).request().get();
        assertEquals(400, response.getStatus());
        assertEquals("{\"error_code\":400,\"error_reason\":\"dep_sid query parameter is mandatory\"}", response.readEntity(String.class));
    }

    @Test
    public void testMissingArrSidParam() {
        Response response = target.path("direct").queryParam("dep_sid", 745).request().get();
        assertEquals(400, response.getStatus());
        assertEquals("{\"error_code\":400,\"error_reason\":\"arr_sid query parameter is mandatory\"}", response.readEntity(String.class));
    }

    @Test
    public void testInvalidDepSidParam() {
        Response response = target.path("direct").queryParam("dep_sid", "xyz").queryParam("arr_sid", 179).request().get();
        assertEquals(400, response.getStatus());
        assertEquals("{\"error_code\":400,\"error_reason\":\"dep_sid query parameter is not an integer\"}", response.readEntity(String.class));
    }

    @Test
    public void testInvalidArrSidParam() {
        Response response = target.path("direct").queryParam("dep_sid", 12).queryParam("arr_sid", "xyz").request().get();
        assertEquals(400, response.getStatus());
        assertEquals("{\"error_code\":400,\"error_reason\":\"arr_sid query parameter is not an integer\"}", response.readEntity(String.class));
    }

    @Test
    public void testSameDepSidArrSidParam() {
        Response response = target.path("direct").queryParam("dep_sid", 12).queryParam("arr_sid", 12).request().get();
        assertEquals(400, response.getStatus());
        assertEquals("{\"error_code\":400,\"error_reason\":\"Departure and arrival stations cannot be same\"}", response.readEntity(String.class));
    }
}
